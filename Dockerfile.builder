FROM golang:1.21.3-alpine
ARG WITNESS_VERSION=0.4.0-beta
RUN apk --no-cache add curl docker make python3 py3-pip gcc python3-dev musl-dev

# Install Witness
RUN curl -sSfL https://github.com/testifysec/witness/releases/download/v${WITNESS_VERSION}/witness_${WITNESS_VERSION}_linux_amd64.tar.gz -o witness.tar.gz && \
    tar -xzvf witness.tar.gz -C /usr/local/bin/ && \
    rm ./witness.tar.gz

# Install Syft and Grype
RUN curl -sSfL https://raw.githubusercontent.com/anchore/syft/main/install.sh | sh -s -- -b /usr/local/bin
RUN curl -sSfL https://raw.githubusercontent.com/anchore/grype/main/install.sh | sh -s -- -b /usr/local/bin

# Install Trufflehog
RUN curl -sSfL https://raw.githubusercontent.com/trufflesecurity/trufflehog/main/scripts/install.sh | sh -s -- -b /usr/local/bin

# Install hadolint
RUN curl -sSfL https://github.com/hadolint/hadolint/releases/download/v2.12.0/hadolint-Linux-x86_64 -o /usr/local/bin/hadolint && chmod +x /usr/local/bin/hadolint

# Install semgrep
RUN python3 -m pip install semgrep
